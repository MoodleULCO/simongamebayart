Vue.component('current-level', {
    props: [
        'level'
    ],
    template: "<span>Level {{ level }}</span>"
})

Vue.component('start-stop', {
    props: [
        'running'
    ],
    computed: {
        label: function() {
            if(this.running) {
                return "Stop";
            } else {
                return "Start";
            }
        }
    },
    template: "<button>{{ label }}</button>"
})

Vue.component('color-button', {
    props: [
        'color',
        'isactive'
    ],
    computed: {
        currentClass: function() {
            if(this.isactive) {
                return this.color + " active";
            } else {
                return this.color;
            }
        }
    },
    methods: {
        switchActive: function () {
            this.isactive = !this.isactive
        }
    },
    template: '<button v-bind:class="currentClass"></button>'
})

new Vue({
    el: '#app',
    data: {
        running: false,
        showTab: false,
        level: 1,
        buttons: [
            { isActive: false, color: "red" },
            { isActive: false, color: "green" },
            { isActive: false, color: "blue" },
            { isActive: false, color: "yellow" }
        ],
        series: [],
        inputSeries: [],
        colorsPlayed: 0,
        sounds: [
          new Audio('https://s3.amazonaws.com/freecodecamp/simonSound1.mp3'),
          new Audio('https://s3.amazonaws.com/freecodecamp/simonSound2.mp3'),
          new Audio('https://s3.amazonaws.com/freecodecamp/simonSound3.mp3'),
          new Audio('https://s3.amazonaws.com/freecodecamp/simonSound4.mp3')
        ]
    },
    methods: {
        switchActive: function(index) {
            this.colorsPlayed = this.colorsPlayed + 1;
            this.series.push(index);
            this.checkPlay();
            this.switchColor(index, 0);
            this.switchColor(index, 200);
            this.playSound(index);
        },
        playSound: function(index) {
            this.sounds[index].play();
        },
        startStop: function() {
            if(this.running) {
                this.stop();
            } else {
                this.start();
            }
        },
        start: function() {
            this.running = true;
            this.showTab = true;
            this.inputSeries = [];
            this.playLevel();
        },
        stop: function() {
            this.running = false;
            this.showTab = false;
            this.level = 1;
            for(let i = 0; i < this.buttons.length; i++) {
                this.buttons[i].isActive = false;
            }
            this.colorsPlayed = 0;
        },
        checkSeries: function() {
            for(let i = 0; i < this.level; i++) {
                console.log("Serie " + this.series[i] + " Input " + this.inputSeries[i])
                if(this.series[i] != this.inputSeries[i]) {
                    return false;
                }
            }
            return true;
        },
        checkSelection: function(index) {
            return this.series[index] != this.inputSeries[index];
        },
        playLevel: function() {
            this.series = [];
            this.colorsPlayed = 0;
            this.generateSeries();
            this.showSeries();
        },
        generateSeries: function() {
            var button = Math.floor(Math.random() * Math.floor(4));
            this.inputSeries.push(button);
        },
        checkPlay: function () {
            if(this.colorsPlayed == this.level) {
                if(this.checkSeries()) {
                    this.level = this.level + 1;
                    setTimeout(() => { this.playLevel() }, 300);
                } else {
                    alert("Perdu ! Votre score est de " + (this.level-1));
                    this.stop();
                }
            }
        },
        showSeries: function() {
            for(let i = 0; i < this.inputSeries.length; i++) {
                var index = this.inputSeries[i];
                this.switchColor(index, 0 + 300*i);
                this.switchColor(index, 400 + 300*i);
            }
        },
        switchColor: function(index, lapse) {
            setTimeout(() => { this.buttons[index].isActive = !this.buttons[index].isActive; }, lapse);
        }
    }
})